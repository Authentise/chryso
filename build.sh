#!/bin/bash -vx
set -e
rm -f dist/*

./pylint.sh
py.test --cov-report xml --cov chryso --junitxml dist/results.xml tests

echo "Building the python package"
python setup.py sdist upload
