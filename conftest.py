import logging
import os
from uuid import uuid4

import attrdict
import pytest
import sqlalchemy

import tests.tables


def pytest_cmdline_main():
    logging.basicConfig(format='[%(asctime)s] %(levelname)s pid:%(process)d thread:%(thread)s %(name)s:%(lineno)d %(message)s')
    logging.getLogger().setLevel(logging.DEBUG)

@pytest.fixture(scope='session')
def metadata():
    return sqlalchemy.MetaData()

@pytest.fixture(scope='session')
def tables():
    return tests.tables

@pytest.fixture(scope='session')
def table(tables): # pylint: disable=redefined-outer-name
    return tables.TestTable

@pytest.fixture(scope='session')
def db_connection_uri():
    return os.environ['DB_URI']

@pytest.fixture(scope='function')
def uuid():
    return uuid4()

@pytest.fixture
def row(db, uuid, insert_row, get_row, tables): # pylint: disable=redefined-outer-name
    insert_row(db, uuid, 'some-value')
    return get_row(db, tables.TestTable, uuid)

@pytest.fixture
def insert_row(tables): # pylint: disable=redefined-outer-name
    def _insert_row(db, uuid, value): # pylint: disable=redefined-outer-name
        query = tables.TestTable.insert().values(uuid=str(uuid), foo=value)
        db.execute(query)
    return _insert_row

@pytest.fixture
def get_row():
    def _get_row(db, table, uuid): # pylint: disable=redefined-outer-name
        query = sqlalchemy.select([table]).where(table.c.uuid==uuid)
        return attrdict.AttrDict(db.execute(query).first())
    return _get_row

@pytest.fixture
def all_rows(tables): # pylint: disable=redefined-outer-name
    def _all_rows(db, table=None): # pylint: disable=redefined-outer-name
        query = sqlalchemy.select([table or tables.TestTable])
        return db.execute(query).fetchall()
    return _all_rows
