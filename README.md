Chryso
------

Chyrso is a layer on top of SQLAlchemy that is designed to make it easier to write applications that leverage the power of Postgres.

It is an outgrowth of our work with SQLAlchemy. SQLAlchemy is excellent, but we found that we kept building similar structures on top of it and wanted some way of sharing those structures between projects. Chryso will grow organically as we need it, but for now it has three major components:

 * Database connection/transaction management
 * Error parsing
 * Filter-to-query translation

Built for python3.x

See CONTRIBUTING.md for build/devl info
