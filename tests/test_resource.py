import attrdict
import pytest
import sqlalchemy

import chryso.errors
import tests.tables
from chryso.resource import Resource


class TestResource(Resource):
    TABLE = tests.tables.TestTable

class Table4Resource(Resource):
    TABLE = tests.tables.Table4

def test_by_uuid(row, uuid):
    assert TestResource.by_uuid(uuid) == row

@pytest.mark.usefixtures("db")
def test_by_uuid_record_not_found(uuid):
    with pytest.raises(chryso.errors.RecordNotFound):
        TestResource.by_uuid(uuid)

def test_by_filter(row):
    update_filters_called = False

    class TestResourceUpdateFilters(Resource):
        TABLE = tests.tables.TestTable

        @staticmethod
        def update_filters(filters):
            nonlocal update_filters_called
            update_filters_called = True

    results = TestResourceUpdateFilters.by_filter({ 'foo': row.foo })
    assert update_filters_called is True
    for result in results:
        assert result == row

def test_override_base_query(db, tables):
    class TestResourceBaseQuery(Resource):
        TABLE = tests.tables.Table1

        @classmethod
        def _get_base_query(cls):
            return sqlalchemy.select([tests.tables.Table1.c.id])
    query = tables.Table1.insert().values(
        id=2,
        foo='abd',
    )
    db.execute(query)
    results = TestResourceBaseQuery.by_filter({'id': 2})
    assert 'foo' not in results[0]

def test_create(db, uuid, table, get_row):
    row = attrdict.AttrDict({
        "uuid"    : uuid,
        "foo"     : "bar",
        "created" : None,
        "updated" : None,
        "deleted" : None,
    })
    assert TestResource.create(**row) == uuid
    result = get_row(db, table, uuid)
    row.created = result.created
    row.updated = result.updated
    assert result == row

def test_update(db, uuid, row, get_row, table):
    row.foo = "foo"
    TestResource.update(uuid,
        foo=row.foo,
        deleted="deleted",
        created="created",
        updated="updated",
    )
    assert get_row(db, table, uuid) == row

@pytest.mark.usefixtures("db", "row")
def test_delete(uuid):
    TestResource.delete(uuid)
    with pytest.raises(chryso.errors.RecordNotFound):
        TestResource.by_uuid(uuid)

@pytest.mark.usefixtures("db")
@pytest.mark.parametrize('filter_', [
    {'foo': 'bar'},
    {'foo': ['bar']},
    {'foo': chryso.queryadapter.FilterArgument('=', ['bar'])},
    {'foo': [chryso.queryadapter.FilterArgument('=', ['bar'])]},
])
def test_filter_simple_string(filter_):
    "Test the different permutations for specifying a filter on a single string"
    TestResource.create(foo='bar')
    TestResource.create(foo='baz')
    results = TestResource.by_filter(filter_)
    assert len(results) == 1
    assert results[0].foo == 'bar'

@pytest.mark.usefixtures("db")
@pytest.mark.parametrize('filter_', [
    {'foo': ['bar', 'biff']},
    {'foo': chryso.queryadapter.FilterArgument('=', ['bar', 'biff'])},
    {'foo': [chryso.queryadapter.FilterArgument('=', ['bar', 'biff'])]},
])
def test_filter_simple_or_string(filter_):
    "Test that we can do a basic filter with ORing two strings"
    TestResource.create(foo='bar')
    TestResource.create(foo='baz')
    TestResource.create(foo='biff')
    results = TestResource.by_filter(filter_)
    assert len(results) == 2
    assert results[0].foo in ('bar', 'biff')
    assert results[1].foo in ('bar', 'biff')

@pytest.mark.usefixtures('db')
@pytest.mark.parametrize('filter_', [
    {'status': ['queued', 'complete']},
    {'status': chryso.queryadapter.FilterArgument('=', [
        tests.tables.TestStatus.complete,
        'queued',
    ])}
])
def test_filter_enums(filter_):
    "Test that we can properly filter by enum"
    Table4Resource.create(status='queued')
    Table4Resource.create(status='in-progress')
    Table4Resource.create(status=tests.tables.TestStatus.complete)
    results = Table4Resource.by_filter(filter_)
    assert len(results) == 2
    assert results[0].status in ('queued', 'complete')
    assert results[1].status in ('queued', 'complete')
