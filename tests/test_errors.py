from uuid import uuid4

import pytest
import sqlalchemy.exc

import chryso.errors


def test_data_length_error(db, insert_row):
    with pytest.raises(sqlalchemy.exc.DataError) as e:
        insert_row(db, uuid4(), 'way too long'*20)
    assert isinstance(e.value, chryso.errors.DataError)
    assert isinstance(e.value, chryso.errors.ValueTooLongError)
    assert e.value.column_type == 'character varying(10)'

def test_duplicate_key(db, insert_row):
    with pytest.raises(sqlalchemy.exc.IntegrityError) as e:
        _uuid = uuid4()
        insert_row(db, _uuid, 'first')
        insert_row(db, _uuid, 'second')
    assert isinstance(e.value, chryso.errors.IntegrityError)
    assert isinstance(e.value, chryso.errors.DuplicateKeyError)
    assert e.value.constraint_name == 'pk_test'
    assert e.value.column_name == 'uuid'
    assert e.value.value == str(_uuid)

def test_fk_constraint_error(db, tables):
    uuid = uuid4()
    query = tables.ChildTable.insert().values(uuid=str(uuid), test_table=str(uuid))
    with pytest.raises(sqlalchemy.exc.IntegrityError) as e:
        db.execute(query)
    assert isinstance(e.value, chryso.errors.IntegrityError)
    assert isinstance(e.value, chryso.errors.ForeignKeyError)
    assert e.value.table_name       == 'childtable'
    assert e.value.constraint_name  == 'fk_childtable_test_test_table'
    assert e.value.column_name      == 'test_table'
    assert e.value.value            == str(uuid)
    assert e.value.referenced_table == 'test'

def test_check_constraint_error(db, tables):
    # We use the value '3' here because it explicitly violates our silly  constraint
    query = tables.Table3.insert().values(id=1, baz=3)
    with pytest.raises(sqlalchemy.exc.IntegrityError) as e:
        db.execute(query)
    assert isinstance(e.value, chryso.errors.IntegrityError)
    assert isinstance(e.value, chryso.errors.CheckConstraintError)
    assert e.value.table_name       == 'table3'
    assert e.value.constraint_name  == 'ck_table3_CK_BAZ'
    assert e.value.row_values       == '(1, 3)'

def test_data_type_error(db, insert_row):
    with pytest.raises(sqlalchemy.exc.DataError) as e:
        insert_row(db, 'not a uuid', '')
    assert isinstance(e.value, chryso.errors.DataError)
