from sqlalchemy import (INTEGER, CheckConstraint, Column, DateTime, ForeignKey,
                        String, Table, func)
from sqlalchemy.dialects.postgresql import ENUM, UUID

from chryso.schema import BaseEnum, metadata, table

TestTable = table('test',
    Column('foo', String(10), nullable=True),
)

ChildTable = table('childtable',
    Column('test_table', UUID(), ForeignKey(TestTable.c.uuid)),
)

Table1 = Table('table1', metadata,
    Column('id', INTEGER(), primary_key=True, nullable=False),
    Column('foo', String(length=10), primary_key=False),
    Column('bar', String(length=10), default=None, primary_key=False),
    Column('created', DateTime, server_default=func.now(), nullable=False),
)

Table2 = Table('table2', metadata,
    Column('id', INTEGER(), primary_key=True, nullable=False),
    Column('bar', String(length=10), primary_key=False),
    Column('created', DateTime, server_default=func.now(), nullable=False),
)

Table3 = Table('table3', metadata,
    Column('id', INTEGER(), primary_key=True, nullable=False),
    Column('baz', INTEGER(), primary_key=False),
    CheckConstraint('(BAZ < 3)', name='CK_BAZ'),
)


class TestStatus(BaseEnum):
    queued      = 'queued'
    in_progress = 'in-progress'
    complete    = 'complete'

StatusEnum = ENUM(*[str(value) for value in TestStatus], name='status_enum')
Table4 = table('table4',
    Column('status', StatusEnum),
)
