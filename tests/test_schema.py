import chryso.schema

class MyEnum(chryso.schema.BaseEnum):
    item = 'item'

def test_enum_in_dict():
    assert {MyEnum.item: True}

def test_enum_repr():
    assert repr(MyEnum.item) == 'item'
