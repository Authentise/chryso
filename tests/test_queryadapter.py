# pylint: disable=line-too-long
import string

import pytest
import sqlalchemy

import chryso.queryadapter

FA = chryso.queryadapter.FilterArgument
QP = chryso.queryadapter.QueryParams

@pytest.mark.parametrize('_filter, expected', [
    ({'id': [FA('>',  [2])]},                               [{'id': 3, 'foo': 'xyz'}]),
    ({'id': [FA('<',  [2])]},                               [{'id': 1, 'foo': 'abc'}]),
    ({'id': [FA('<=', [-1])]},                              []),
    ({'id': [FA('=',  [1])], 'foo': [FA('=', ['def'])]},    []),
    ({'id': [FA('<=', [2])], 'foo': [FA('=', ['def'])]},    [{'id': 2, 'foo': 'def'}]),
    ({'id': [FA('<=', [2])]},                               [{'id': 1, 'foo': 'abc'}, {'id': 2, 'foo': 'def'}]),
    ({'id': [FA('<=', [2]), FA('>', [1])]},                 [{'id': 2, 'foo': 'def'}]),
])
def test_queryadapter_simple_types(_filter, db, expected, tables):
    rows = [
        {'id'    : 1, 'foo': 'abc'},
        {'id'    : 2, 'foo': 'def'},
        {'id'    : 3, 'foo': 'xyz'},
    ]
    db.execute(tables.Table1.insert(), rows)
    select = sqlalchemy.sql.select([tables.Table1.c.id, tables.Table1.c.foo])

    mapped = chryso.queryadapter.map_column_names(tables.Table1, _filter)
    query = chryso.queryadapter.apply_filter(select, mapped)
    rows = db.execute(query).fetchall()
    results = [dict(row) for row in rows]
    assert results == expected

# Use lambda's here because we need to use the test_table fixture and can't parametrize it directly
@pytest.mark.parametrize('gettable', [
    lambda x: x,
    lambda x: [x],
])
def test_queryadapter_map_column_names_happy_path_single(gettable, tables):
    _filter = {
        'foo'     : 'a',
    }
    _map = chryso.queryadapter.map_column_names(gettable(tables.Table1), _filter)
    assert _map == {tables.Table1.c.foo: 'a'}

def test_queryadapter_map_column_names_happy_path_triple(tables):
    _filter = {
        'foo'     : 'a',
        'id'      : 1,
        'created' : '2015-01-01',
    }
    _map = chryso.queryadapter.map_column_names([tables.Table1], _filter)
    assert _map == {
        tables.Table1.c.id      : 1,
        tables.Table1.c.foo     : 'a',
        tables.Table1.c.created : _filter['created']
    }

def test_queryadapter_map_column_names_alias(tables):
    _filter = {
        'id'            : 1,
        'foobar'        : 'a',
        'created'       : '2015-01-01',
        'created_alias' : '2015-01-02',
    }
    foobar = tables.Table1.c.foo.label('foobar')
    created_alias = tables.Table1.c.created.label('created_alias')
    _map = chryso.queryadapter.map_column_names([tables.Table1, foobar, created_alias], _filter)
    assert _map == {
        tables.Table1.c.id      : 1,
        foobar                  : 'a',
        tables.Table1.c.created : _filter['created'],
        created_alias           : _filter['created_alias']
    }

def test_queryadapter_map_column_names_ambiguous(tables):
    _filter = {
        'id'    : 1,
        'foo'   : 'a',
    }
    with pytest.raises(Exception) as e:
        chryso.queryadapter.map_column_names([tables.Table1, tables.Table2], _filter)

    assert "Ambiguous column reference 'id'. It matches both table1.id and table2.id" in str(e)

def test_queryadapter_map_column_names_invalid_key(tables):
    _filter = {'unkonwn'   : 1,}
    with pytest.raises(Exception) as e:
        chryso.queryadapter.map_column_names([tables.Table1], _filter)
    assert "Unrecognized column reference 'unkonwn'. None of the tables provided have a column by that name" in str(e)

@pytest.mark.parametrize('_input, remap, expected', [
    ({'a': 1},          {'a': 'b'},             {'b': 1}),
    ({'a': 1, 'b': 2},  {'a': 'b'},             {'b': 1}),
    ({'a': 1, 'b': 2},  {'a': 'c'},             {'b': 2, 'c': 1}),
    ({'a': 1, 'b': 2},  {},                     {'a': 1, 'b': 2}),
    ({'a': 1},          {'a': 'b', 'c': 'd'},   {'b': 1}),
])
def test_remap_filter(_input, remap, expected):
    result = chryso.queryadapter.remap_filter(_input, remap)
    assert result == expected

@pytest.mark.parametrize('_input, formatter, expected', [
    ({'a': 1},          {'a': lambda v: v+1},               {'a': 2}),
    ({'a': 1, 'b': 2},  {'a': lambda v: 'foo'},             {'a': 'foo', 'b': 2}),
    ({'a': 1},          {'a': lambda v: 'bar', 'b': None},  {'a': 'bar'}),
])
def test_format_filter(_input, formatter, expected):
    result = chryso.queryadapter.format_filter(_input, formatter)
    assert result == expected

def test_map_and_filter(tables):
    filters = {'id': 1}
    select = sqlalchemy.sql.select([tables.Table1])
    result = chryso.queryadapter.map_and_filter(tables.Table1, filters, select)
    assert str(result) == 'SELECT table1.id, table1.foo, table1.bar, table1.created \nFROM table1 \nWHERE table1.id = :id_1'

@pytest.mark.parametrize('queryparams, expected', [
    (QP(orders='foo'), [3, 2, 1]),
    (QP(orders=['+foo']), [3, 2, 1]),
    (QP(orders='-foo'), [1, 2, 3]),
])
def test_order(db, expected, queryparams, tables):
    rows = [
        {'id'    : 1, 'foo': 'z'},
        {'id'    : 2, 'foo': 'y'},
        {'id'    : 3, 'foo': 'x'},
    ]
    db.execute(tables.Table1.insert(), rows)
    select = sqlalchemy.sql.select([tables.Table1.c.id, tables.Table1.c.foo])
    query = chryso.queryadapter.apply_params(tables.Table1, queryparams, select)
    rows = db.execute(query).fetchall()
    results = [row['id'] for row in rows]
    assert results == expected

@pytest.mark.parametrize('queryparams, expected', [
    (QP(orders=['foo', '-bar']), [1, 3, 2]),
    (QP(orders=['-foo', '+bar']), [2, 3, 1])
])
def test_order_layered(db, expected, queryparams, tables):
    "Ensure we can put multiple orderings together"
    rows = [
        {'id'    : 1, 'foo': 'a', 'bar': 'x'},
        {'id'    : 2, 'foo': 'b', 'bar': 'x'},
        {'id'    : 3, 'foo': 'b', 'bar': 'y'},
    ]
    db.execute(tables.Table1.insert(), rows)
    select = sqlalchemy.sql.select([tables.Table1.c.id, tables.Table1.c.foo])
    query = chryso.queryadapter.apply_params(tables.Table1, queryparams, select)
    rows = db.execute(query).fetchall()
    results = [row['id'] for row in rows]
    assert results == expected

@pytest.mark.parametrize('offset, limit, expected', [
    (None, None, [c for c in string.ascii_letters]),
    (10, None, [c for c in string.ascii_letters[10:]]),
    (None, 20, [c for c in string.ascii_letters[:20]]),
    (15, 22, [c for c in string.ascii_letters[15:15+22]]),
    (15, 4, [c for c in string.ascii_letters[15:15+4]]),
])
def test_limit_and_offset(db, expected, limit, offset, tables):
    rows = [{'id': i, 'foo': c} for i, c in enumerate(string.ascii_letters)]
    db.execute(tables.Table1.insert(), rows)
    select = sqlalchemy.sql.select([tables.Table1.c.id, tables.Table1.c.foo])
    queryparams = QP(limit=limit, offset=offset)
    query = chryso.queryadapter.apply_params(tables.Table1, queryparams, select)
    rows = db.execute(query).fetchall()
    results = [row['foo'] for row in rows]
    assert results == expected
