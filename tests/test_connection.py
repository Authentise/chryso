import random
import threading
import time
from uuid import uuid4

import pytest
import sqlalchemy
import sqlalchemy.exc

import chryso.connection
import chryso.errors


def test_query_count(all_rows, db):
    "Test that we can accurately track the query count"
    db.reset_queries()
    results = all_rows(db)
    assert results == []
    assert len(db.queries) == 1

def test_tracking_queries_default(db_connection_uri, tables):
    "Ensure that by default we do not track queries"
    engine = chryso.connection.Engine(db_connection_uri, tables)
    engine.execute('select now()')
    assert engine.queries == []

def test_set_timezone_default(db_connection_uri, tables):
    "Ensure that by default we set timezone to utc."
    engine = chryso.connection.Engine(db_connection_uri, tables)
    result = engine.execute('show timezone').first()
    assert result[0] == 'UTC'

def test_autocommit(all_rows, db_raw, insert_row):
    "Ensure that we are in autocommit mode since we can insert rows, error, then keep inserting"
    assert not all_rows(db_raw)
    insert_row(db_raw, uuid4(), '1')
    assert len(all_rows(db_raw)) == 1
    _uuid = uuid4()
    insert_row(db_raw, _uuid, '2')
    with pytest.raises(chryso.errors.DuplicateKeyError):
        insert_row(db_raw, _uuid, '3')
    assert len(all_rows(db_raw)) == 2
    assert len(db_raw.queries) == 6

def test_transaction_atomicity_flat(all_rows, db_raw, insert_row):
    "Ensure that an 'atomic' block keeps actions together in a transaction"
    assert not all_rows(db_raw)
    insert_row(db_raw, uuid4(), '1')
    assert len(all_rows(db_raw)) == 1

    with pytest.raises(sqlalchemy.exc.DataError) as e:
        with db_raw.atomic():
            insert_row(db_raw, uuid4(), '2')
            insert_row(db_raw, uuid4(), 'way too long'*20)
    assert 'value too long' in str(e.value)

    assert len(all_rows(db_raw)) == 1
    insert_row(db_raw, uuid4(), '2')
    assert len(all_rows(db_raw)) == 2
    assert len(db_raw.queries) == 10

def test_transaction_atomicity_nested(all_rows, db_raw, insert_row):
    "Ensure that inside a nested 'atomic' block we roll back savepoints"
    insert_row(db_raw, uuid4(), '0')
    with pytest.raises(Exception):
        with db_raw.atomic():
            insert_row(db_raw, uuid4(), '1')
            assert len(all_rows(db_raw)) == 2

            with pytest.raises(sqlalchemy.exc.DataError) as e:
                with db_raw.atomic():
                    insert_row(db_raw, uuid4(), '2')
                    insert_row(db_raw, uuid4(), 'way too long'*20)
            assert 'value too long' in str(e.value)

            assert len(all_rows(db_raw)) == 2
            insert_row(db_raw, uuid4(), '2')
            assert len(all_rows(db_raw)) == 3
            raise Exception("Forcing error out")
    assert len(all_rows(db_raw)) == 1

def test_engine_atomic_works_nested(all_rows, db_raw, insert_row):
    "Test that we can call atomic inside atomic on the engine and rollback works nested"
    assert not all_rows(db_raw)
    with db_raw.atomic():
        insert_row(db_raw, uuid4(), '1')
        assert len(all_rows(db_raw)) == 1
        with pytest.raises(chryso.errors.DuplicateKeyError):
            with db_raw.atomic():
                _uuid = uuid4()
                insert_row(db_raw, _uuid, '2')
                assert len(all_rows(db_raw)) == 2
                insert_row(db_raw, _uuid, '3')
        assert len(all_rows(db_raw)) == 1
    assert len(all_rows(db_raw)) == 1

def test_commit_on_savepoints(all_rows, db_raw, insert_row):
    "Test that when using nested atomic blocks where some commit we properly manipulate savepoints"
    assert not all_rows(db_raw)
    with db_raw.atomic():
        with db_raw.atomic():
            insert_row(db_raw, uuid4(), '1')
        assert len(all_rows(db_raw)) == 1
        with pytest.raises(chryso.errors.DuplicateKeyError):
            with db_raw.atomic():
                _uuid = uuid4()
                insert_row(db_raw, _uuid, '2')
                insert_row(db_raw, _uuid, '2b')
        assert len(all_rows(db_raw)) == 1
        insert_row(db_raw, _uuid, '3')
        with pytest.raises(chryso.errors.DuplicateKeyError):
            insert_row(db_raw, _uuid, '3b')
    assert not all_rows(db_raw)
    # At this point we should have rolled back twice, but both in savepoints
    # If we didn't, the next block will have trouble finishing properly as it
    # tries to rollback a non-existent savepoint
    with db_raw.atomic():
        insert_row(db_raw, uuid4(), '4')
    assert len(all_rows(db_raw)) == 1

def test_explicit_rollback(all_rows, db_raw, insert_row):
    "Test that we can explicitly call rollback during an atomic and get the right behavior"
    assert not all_rows(db_raw)
    with db_raw.atomic() as session:
        insert_row(db_raw, uuid4(), '1')
        session.rollback()
    assert not all_rows(db_raw)

def _make_engine(db_connection_uri):
    return chryso.connection.Engine(
        db_connection_uri,
        None,
        echo=None,
        track_queries=False,
    )

def _add_some_rows(engine, i, insert_row):
    with engine.atomic():
        for x in range(3):
            insert_row(engine, uuid4(), "{}.o{}".format(i, x))
            time.sleep(random.random() / 10.)
        with engine.atomic() as transaction:
            for x in range(3):
                insert_row(engine, uuid4(), "{}.i{}".format(i, x))
                time.sleep(random.random() / 10.)
            transaction.rollback()

def test_atomic_with_threading_shared_engine(db_raw, db_connection_uri, insert_row):
    """
    Ensure that we can do random, chaotic stuff with multiple threads including
    rollbacks and we still get all of our data intact while not sharing the engine
    """
    engine = _make_engine(db_connection_uri)
    threads = [
        threading.Thread(target=_add_some_rows, kwargs={
            'engine'            : engine,
            'i'                 : i,
            'insert_row'        : insert_row,
        }) for i in range(8)
    ]
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()
    rows = db_raw.execute("SELECT COUNT(*) FROM test").first()
    assert rows[0] == 24
