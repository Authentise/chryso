from uuid import uuid4

import pytest

import chryso.connection


@pytest.mark.parametrize('execution', [1, 2])
def test_state_bleedover(all_rows, db, execution, insert_row):
    assert all_rows(db) == []
    insert_row(db, uuid4(), str(execution))
    assert len(all_rows(db)) == 1

def test_chryso_connection_stored_raw(db_raw):
    assert chryso.connection.get() == db_raw

def test_chryso_connection_stored_transaction(db):
    assert chryso.connection.get() == db
