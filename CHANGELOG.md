Changelog
---------

2.1
---
Improve the drop_all function to handle circular references

2.0
---
This change brings a new commitment to ensuring that sepiida and chryso can interact seamlessly. I've updated the parameters and behavior around
queryadapter.apply_params so that you can easily build a QueryParams object and pass it through with minimal changes and things like offset, limit and sorts are applied
correctly. There may be more breaking changes going forward to make this work so this is the start of a new major series

1.27
----
Update to SQLAlchemy 1.1.11

1.26
----
Update alembic to 0.9.2

1.25
----
Previously any Resource would prevent the creation of a new resource that included 'created', 'updated' or 'deleted' via
the _sanitize_kwargs function on the Resouce class. That is still in place, but now descendent classes can change the fields
that are excluded via the PROPERTY_CREATION_BLACKLIST property on the class

1.24
----
Add support for order and limit clauses

1.23
----
Add log message when creating or updating resources so we automatically get more traceability

1.22
----
Remove atomic semaphore in favor of a thread pool that is aware of threads and just creates a new connection per thread

1.21
----
Log a warning if a thread waits on the atomic semaphore for 30 seconds. This is useful for debugging semaphore lockout

1.20
----
Do not allow multiple threads to enter atomic blocks at the same time. We use a simple semaphore to prevent this. It's important because without it
we get transaction state bleeding over across threads

1.19
----
Convert Enums when creating, updating and filtering resources automatically. Also handle filters that are not specified as an iterable but *are* a FilterArgument (or equivalent).

1.18
----
repr(Enum) is now equivalent to str(Enum). This may help with serialization. May not.

1.17
----
Additional logs around transaction management at the DEBUG level

1.16
----
Fix installation on windows when git isn't installed or in the PATH

1.15
----
Update to latest psycopg2 - 2.6.2. This is my attempt to get chryso installed on a windows machine...

1.14
----
Allow hashing of Enum so it can be used as a key in dicts

1.13
----
Auto push updates to PyPI

1.12
----
Allow users of the Resource class to override how the base query is created. Previously this was contained within a function doing other work. Now it's in a separate function so it can be easily overridden

1.11
---
Change the default naming convention for uniqueness constraints so that we can continue to use Column(..., unique=True). Without this change (so in the entire 1 series) we get an unhandled exception from sqlalchemy if we define a column with this option enabled. That's bad.

The new logic looks at the number of columns and generates the constraint name deterministically, but dynamically, so it can properly be backwards compatible and handle the unique=True paramter
1.10
----
Add support for parsing check constraint errors

1.9
---
Add arrow to list of dependecies. Also change apply_filter to accept FilterArgument for _filter argument. This lets us query between dates.

Depending on how your client uses the queryfilter module you may need to change the way the function is getting called. Going forward you'll need to be sure that you are passing in a dictionary with a key/value mapping
of columns to lists. The list contains something like a FilterArgument and ANDs them together. Within a FilterAgrument the values property is ORd together with the given operation.

1.8
---
Add pycountry package to list of dependencies. It can be shared between applications to get a list of countries and currencies.

1.7
---
Set TimeZone to UTC for all connections.

1.6
---
Changed METADATA to metadata in chryso.schema.

Fixed primary key to have a posgres server default which allows the database to generate a uuid on the column default.

1.5
---
Add Resource platform layer base class and schema table helper

1.3
---
Fix memory leak by not honoring the `track_query` parameter to Engine()

1.2
---
Refactor session handling logic to separate out sessions from transactions

1.1
---
Reworked session support so that atomic() works on engines themselves and our db fixture returns the db engine rather than a session. This means that some interactions with the fixtures may need to change in clients of the library.

I pulled away from the SQLAlchemy Session implementation and rolled my own after a long, frustrating session trying to get the behavior I want out of SQLAlchemy with psycopg2

1.0
---

Initial release
