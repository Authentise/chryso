#!/usr/bin/env fish
set PROJECT 'chryso'
mkdir -p /dist
set SOURCEFILES (find $PROJECT -maxdepth 3 -name "*.py" -print)
set TESTFILES (find tests -maxdepth 3 -name "*.py" -print)
set FILES setup.py $SOURCEFILES $TESTFILES
echo $FILES
if not pylint $FILES --reports=no | tee /dist/pylint.log
	echo "Linting failed"
	exit 1
end
