# Requirements
Before you start using this project you need to have the following installed
 - python 3.5+
 - SQLAlchmey installed 
 - PostgreSQL Installed

 - Create a db
    createdb chryso_unit_test_db 


#Develop:
Please work and test in a virtualenv, to avoid python pollution. 

 1. Setup a virtualenv
 - see website for details

 2. Launch virtual env
 - see website for details
 3. install as developer 
   run 'python3 setup.py develop' to install requiremnets to develop
 5. set env.

 4 run unit tests: 
	export DB_URI=postgresql://localhost/chryso_unit_test_db                                                                  
	'py.test'  can be used to run unit-tests on the codebase
	'./pylint.sh' or './pylint.fish' should be used to check code standards

#Clean up
  dropdb chryso_unit_test_db
